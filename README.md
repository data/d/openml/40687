# OpenML dataset: solar-flare

https://www.openml.org/d/40687

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Gary Bradshaw    
**Source**: [UCI](http://archive.ics.uci.edu/ml/datasets/solar+flare)   
**Please cite**:   

**Solar Flare database**
Relevant Information:
    -- The database contains 3 potential classes, one for the number of times a
       certain type of solar flare occured in a 24 hour period.
    -- Each instance represents captured features for 1 active region on the 
       sun.
    -- The data are divided into two sections. The second section (flare.data2)
       has had much more error correction applied to the it, and has 
       consequently been treated as more reliable.
 
Number of Instances:  flare.data1: 323, flare.data2: 1066
 
Number of attributes:  13 (includes 3 class attributes)
 
### Attribute Information
    1. Code for class (modified Zurich class)  (A,B,C,D,E,F,H)
    2. Code for largest spot size              (X,R,S,A,H,K)
    3. Code for spot distribution              (X,O,I,C)
    4. Activity                                (1 = reduced, 2 = unchanged)
    5. Evolution                               (1 = decay, 2 = no growth, 
                                                3 = growth)
    6. Previous 24 hour flare activity code    (1 = nothing as big as an M1,
                                                2 = one M1,
                                                3 = more activity than one M1)
    7. Historically-complex                    (1 = Yes, 2 = No)
    8. Did region become historically complex  (1 = yes, 2 = no) 
       on this pass across the sun's disk
    9. Area                                    (1 = small, 2 = large)
   10. Area of the largest spot                (1 = <=5, 2 = >5)
 
  From all these predictors three classes of flares are predicted, which are 
  represented in the last three columns.
 
   11. C-class flares production by this region    Number  
       in the following 24 hours (common flares)
   12. M-class flares production by this region    Number
       in the following 24 hours (moderate flares)
   13. X-class flares production by this region    Number
       in the following 24 hours (severe flares)
 
 CLASSTYPE: nominal
 CLASSINDEX: first

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/40687) of an [OpenML dataset](https://www.openml.org/d/40687). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/40687/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/40687/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/40687/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

